import processing.serial.*;
Serial myPort;
String Send;
float delYaw;
float yawROM = 0.0;
float yaw = 0.0;
float pitch = 0.0;
float roll = 0.0;
float yawH = 0.0;
float pitchH = 0.0;
float rollH = 0.0;
float center = 300.0;
float lBanTay=150;
float lNgonTay=50;
float lNgonTayP=80;
int rNgontay=25;
int rPoint=10;
int dNgontays = 3*rNgontay/2;
float   crHp;
float   srHp;
float   cbHp;
float   sbHp;
float   caHp;
float   saHp;
boolean haveData = false;

  float cr ;
  float sr ;
  float cb ;
  float sb ;
  float ca ;
  float sa ;
void setup()
{
  size(600, 600, P3D);


  // if you have only ONE serial port active
  //myPort = new Serial(this, Serial.list()[0], 9600); // if you have only ONE serial port active

  // if you know the serial port name
  myPort = new Serial(this, "COM3", 115200);                    // Windows
  //myPort = new Serial(this, "/dev/ttyACM0", 9600);             // Linux
  //myPort = new Serial(this, "/dev/cu.usbmodem1217321", 9600);  // Mac

  textSize(16); // set text size
  textMode(SHAPE); // set text mode to shape
}
int i=0;
int pit=0;
void draw()
{
  i++;
   if(i%2==0) pit++;
  serialEvent();  // read and parse incoming serial message
  background(255); // set background to white
  stroke(0);
  strokeWeight(1);
  line(0, center, 1000, center);
  line(center, 0, center, 1000);
  lights();
  //set vi tri trung ta,
  camera(mouseX, height/2, (height/2) / tan(PI/6), width/2, height/2, 0, 0, 1, 0);
  translate(width/2,height/2); // set position to centre
  //rotateY(map(mouseX, 0, width, 0, PI));
  //rotateZ(map(mouseY, 0, height, 0, -PI));



  /**********Draw canh tay************/
  pushMatrix(); // begin object  
  canhtayDraw();
  popMatrix(); // end of object
  
  fill(51, 204, 204);
  drawSphare(35);//khop co tay
  /**********Draw long ban tay************/
  pushMatrix(); // begin object
  //float c1 = cos(radians(roll));//G//X
  //float s1 = sin(radians(roll));
  //float c2 = cos(radians(pitch));//B//Y
  //float s2 = sin(radians(pitch));
  //float c3 = cos(radians(yaw));//A //Z
  //float s3 = sin(radians(yaw));
  //float c1 = cos(radians(0));
  //float s1 = sin(radians(0));
  //float c2 = cos(radians(pit));
  //float s2 = sin(radians(pit));
  //float c3 = cos(radians(0));
  //float s3 = sin(radians(0));
   cr = cos(radians(roll));
   sr = sin(radians(roll));
   cb = cos(radians(pitch));
   sb = sin(radians(pitch));
   ca = cos(radians(yaw));
   sa = sin(radians(yaw));
  //Y-Z-X rota
  //applyMatrix( c2*c3, s1*s3+c1*c3*s2,     c3*s1*s2-c1*s3, 0,
  //             -s2,   c1*c2,             c2*s1,           0,
  //             c2*s3, c1*s2*s3-c3*s1,     c1*c3+s1*s2*s3, 0,
  //             0,     0,                   0,             1);
  
    ////theo ma tran mang
    //applyMatrix( ca, -sa,     0, 0,
    //           sa,   ca,            0,           0,
    //           0, 0,   1, 0,
    //           0,     0,                   0,             1);
    //matran P1
        applyMatrix( ca*cb, sr*sb-cr*cb*sa,     cb*sa*sr+cr*sb, 0,
               sa,   ca*cr,             -ca*sr,           0,
               -ca*sb, cb*sr+cr*sa*sb,     cb*cr-sa*sa*sr, 0,
               0,     0,                   0,             1);
       
   /*
   matric thi duoc ap dung cho xoay tai tam window, nhung ta duoi draw thi duoc nhu duoi
   */
  translate(lBanTay/2,0); // set position to centre
  longbantayDraw();
  popMatrix(); // end of object
  /*
  *tim diem cuoi cua long ban tay matranxuay*dodai ban dau
  *diem cuoi do nhan
  */
  
  ///**********Draw ngon tay************/   
  translate(0,0,0);
  motionNgonTayDraw();



  //pushMatrix(); // begin object
  ////translate(0, 200);
  //float crH = cos(radians(rollH));
  //float srH = sin(radians(rollH));
  //float cbH = cos(radians(pitchH));
  //float sbH = sin(radians(pitchH));
  //float caH = cos(radians(yawH));
  //float saH = sin(radians(yawH));
  ////float crH = cos(radians(pit));
  ////float srH = sin(radians(pit));
  ////float cbH = cos(radians(0));
  ////float sbH = sin(radians(0));
  ////float caH = cos(radians(pit));
  ////float saH = sin(radians(pit));
  
  ////dich ngon tay set vi tri
  //translate(lBanTay*ca*cb,lBanTay*sa,-ca*lBanTay*sb);
  //applyMatrix( cbH*caH, srH*saH-crH*caH*sbH, caH*srH*sbH+crH*saH, 0,
  //             sbH, crH*cbH, -cbH*srH, 0,
  //             -cbH*saH, crH*sbH*saH+caH*srH, crH*caH-srH*sbH*saH, 0,
  //             0, 0, 0, 1);
  //translate(lNgonTay/2,0); // set position to centre
  //ngontayDraw();
  //popMatrix(); // end of object
  
  
  pushMatrix(); 
  drawPoint();
  popMatrix(); 
if(haveData){
  //// Print values to console
  //print(rollH);
  //print("\t");
  //print(pitchH);
  //print("\t");
  print(yawH);
  print("\t");print("\t");print("\t");
  //print(roll);
  //print("\t");
  //print(pitch);
  print("\t");
  print(yaw);
  print("\t");
  print(yawROM);
  print("\t");
  println();
  haveData =false;
}
}

void serialEvent()
{
  int newLine = 13; // new line character in ASCII
  String message;
  do {
    message = myPort.readStringUntil(newLine); // read from port until new line
    if (message != null) {
      //print(message);println();
      haveData = true;
      String[] list = split(trim(message), ":");
      if (list.length >= 4 && list[0].equals("Or")) {
        pitch = float(list[1]); // convert to float yaw//H long ban tay
        roll = float(list[2]); // convert to float pitch
        yaw = -float(list[3]); // convert to float roll
        pitchH = float(list[4]); // convert to float yaw
        rollH = float(list[5]); // convert to float pitch
         yawH = -float(list[6]); // convert to float roll
         
         delYaw = yawH-yaw;
         if(delYaw>180) {
           yawROM = 360-delYaw;
         } else {
         yawROM=delYaw;}
        //yaw = float(list[1]); // convert to float yaw
        //pitch = float(list[2]); // convert to float pitch
        //roll = -float(list[3]); // convert to float roll
        //yawH = float(list[4]); // convert to float yaw
        //pitchH = float(list[5]); // convert to float pitch
        //rollH = -float(list[6]); // convert to float roll
      }
      if (list.length >= 4 && list[0].equals("OrH")) {
        yawH = float(list[1]); // convert to float yaw
        rollH = float(list[2]); // convert to float pitch
        pitchH = -float(list[3]); // convert to float roll
      }
      if (list[0].equals("INIT")) {
        Send =  message;
        print(Send);println();
      }
      if (list[0].equals("2IMUOK")) {
        Send =  message;
        print(Send);println();
      }
      if (list[0].equals("Connected")) {
        Send =  message;
        print(Send);println();
      }
    }
  } while (message != null);
}
void motionNgonTayDraw(){
  /**********Draw ngon tay************/
  pushMatrix(); // begin object
  //translate(0, 200);
  float crH = cos(radians(rollH));//X
  float srH = sin(radians(rollH));
  float cbH = cos(radians(pitchH));//Y
  float sbH = sin(radians(pitchH));
  float caH = cos(radians(yawROM));//Z //yawROM
  float saH = sin(radians(yawROM));
   crHp=crH;
   srHp=srH;
   cbHp=cbH;
   sbHp=sbH;
   caHp=caH;
   saHp=saH;
  //float crH = cos(radians(pit));
  //float srH = sin(radians(pit));
  //float cbH = cos(radians(0));
  //float sbH = sin(radians(0));
  //float caH = cos(radians(pit));
  //float saH = sin(radians(pit));
  //translate(-lBanTay*(c1*s3-c3*s1*s2),c2*lBanTay*s1,lBanTay*(c1*c3+s1*s2*s3));
  //dich ngon tay set vi tri
  pushMatrix();
  //translate = P1*khoang cach toi diem do
  translate(lBanTay*ca*cb,lBanTay*sa,-ca*lBanTay*sb);
  /**phuo trinh 3 truc cho  ngon tay**/
  //applyMatrix( cbH*caH, srH*sbH-crH*cbH*saH, cbH*srH*saH+crH*sbH, 0,
  //             saH, crH*caH, -caH*srH, 0,
  //             -caH*sbH, crH*saH*sbH+cbH*srH, crH*cbH-srH*saH*sbH, 0,
  //             0, 0, 0, 1);
               
   ///***phuon grting 1 truc yaw cho ngot ay ****/
   //  applyMatrix( caH, -saH, 0, 0,
   //            saH, caH, 0, 0,
   //            0, 0, 1, 0,
   //            0, 0, 0, 1);
      /***phuon grting T1*RZH ****/
     applyMatrix( saH*(sb*sr - cb*cr*sa) + ca*caH*cb, caH*(sb*sr - cb*cr*sa) - ca*cb*saH, cr*sb + cb*sa*sr, 0,
               caH*sa + ca*cr*saH,                 ca*caH*cr- sa*saH,           -ca*sr, 0,
               saH*(cb*sr + cr*sa*sb) - ca*caH*sb, caH*(cb*sr + cr*sa*sb) + ca*saH*sb, cb*cr - sa*sb*sr, 0,
               0, 0, 0, 1);
  translate(0,0); // set position to centre
  fill(51, 204, 204);
  drawSphare(rNgontay/2);//khop co tay  
  
  ngontayDraw();
  popMatrix(); 
  
  /*tinh voi diem [lBnftay;0;dNgontay]*/
  pushMatrix();
  translate(dNgontays*(cr*sb + cb*sa*sr) + ca*cb*lBanTay,lBanTay*sa - ca*dNgontays*sr,dNgontays*(cb*cr - sa*sb*sr) - ca*lBanTay*sb);
  //applyMatrix( cbH*caH, srH*sbH-crH*cbH*saH, cbH*srH*saH+crH*sbH, 0,
  //             saH, crH*caH, -caH*srH, 0,
  //             -caH*sbH, crH*saH*sbH+cbH*srH, crH*cbH-srH*saH*sbH, 0,
  //             0, 0, 0, 1);
   //applyMatrix( caH, -saH, 0, 0,
   //saH, caH, 0, 0,
   //0, 0, 1, 0,
   //0, 0, 0, 1);
        applyMatrix( saH*(sb*sr - cb*cr*sa) + ca*caH*cb, caH*(sb*sr - cb*cr*sa) - ca*cb*saH, cr*sb + cb*sa*sr, 0,
               caH*sa + ca*cr*saH,                 ca*caH*cr- sa*saH,           -ca*sr, 0,
               saH*(cb*sr + cr*sa*sb) - ca*caH*sb, caH*(cb*sr + cr*sa*sb) + ca*saH*sb, cb*cr - sa*sb*sr, 0,
               0, 0, 0, 1);
  //translate(lNgonTay/2,0); // set position to centre
  //print(lBanTay*(-c1*s3-c3*s1*s2)+">>"+-c2*lBanTay*s1+">>"+lBanTay*(c1*c3-s1*s2*s3)+"\r\n");
  translate(0,0,0); // set position to centre
  
  fill(51, 204, 204);
  drawSphare(rNgontay/2);//khop co tay
  
  ngontayDraw();
  popMatrix();
  
  
  popMatrix(); // end of object  
}
void ngontayDraw()
{
  /* function contains shape(s) that are rotated with the IMU */

  //box(300, 10, 200); // draw Arduino board base shape

  //stroke(0); // set outline colour to black
  //fill(80); // set fill colour to dark grey

  translate(0,0, 0); // set position to edge of Arduino box
  //box(lNgonTay, 10, 30); // draw pin header as box
  rotateZ(-PI/2); //quay ngon tay lai cho dung
  stroke(0, 90, 90); // set outline colour to darker teal
  fill(0, 130, 70); // set fill colour to lighter teal
  drawCylinder(10,10,80,360);
  //translate(-20, 0, -180); // set position to other edge of Arduino box
  //box(210, 20, 10); // draw other pin header as box
}
void longbantayDraw()
{
  /* function contains shape(s) that are rotated with the IMU */
  stroke(0, 90, 90); // set outline colour to darker teal
  fill(0, 130, 130); // set fill colour to lighter teal
  translate(0, 0, 0); // set position to edge of Arduino box
  box(lBanTay, 20, 100); // draw pin header as box

}
void canhtayDraw()
{
  /* function contains shape(s) that are rotated with the IMU */
  stroke(0, 90, 90); // set outline colour to darker teal
  translate(0,0, 0); // set position to edge of Arduino box
  fill(0, 130, 150);
  drawCylinder(30,30,250,6);
  //box(20, 200, 50); // draw Arduino board base shape

}
void drawCylinder(int sides, float r, float h)
{   
    float angle = 360 / sides;
    float halfHeight = h / 2;
    // draw top shape
    beginShape();
    for (int i = 0; i < sides; i++) {
        float x = cos( radians( i * angle ) ) * r;
        float y = sin( radians( i * angle ) ) * r;
        vertex( x, y, -halfHeight );    
    }
    endShape(CLOSE);
    // draw bottom shape
    beginShape();
    for (int i = 0; i < sides; i++) {
        float x = cos( radians( i * angle ) ) * r;
        float y = sin( radians( i * angle ) ) * r;
        vertex( x, y, halfHeight );    
    }
    endShape(CLOSE);
} 
void drawSphare(int r){
noStroke();
lights();
sphere(r); 
}
void drawCylinder(float topRadius, float bottomRadius, float tall, int sides) {
  float angle = 0;
  float angleIncrement = TWO_PI / sides;
  beginShape(QUAD_STRIP);
  for (int i = 0; i < sides + 1; ++i) {
    vertex(topRadius*cos(angle), 0, topRadius*sin(angle));
    vertex(bottomRadius*cos(angle), tall, bottomRadius*sin(angle));
    angle += angleIncrement;
  }
  endShape();
  
  // If it is not a cone, draw the circular top cap
  if (topRadius != 0) {
    angle = 0;
    beginShape(TRIANGLE_FAN);
    
    // Center point
    vertex(0, 0, 0);
    for (int i = 0; i < sides + 1; i++) {
      vertex(topRadius * cos(angle), 0, topRadius * sin(angle));
      angle += angleIncrement;
    }
    endShape();
  }

  // If it is not a cone, draw the circular bottom cap
  if (bottomRadius != 0) {
    angle = 0;
    beginShape(TRIANGLE_FAN);

    // Center point
    vertex(0, tall, 0);
    for (int i = 0; i < sides + 1; i++) {
      vertex(bottomRadius * cos(angle), tall, bottomRadius * sin(angle));
      angle += angleIncrement;
    }
    endShape();
  }
}

void drawPoint(){
  //translate(lNgonTay*(saHp*(sb*sr - cb*cr*sa) + ca*caHp*cb) + ca*ca*cb*cb*lBanTay + lBanTay*sa*(sb*sr - cb*cr*sa) - ca*lBanTay*sb*(cr*sb + cb*sa*sr),
  //lNgonTay*(caHp*sa + ca*cr*saHp) + ca*cb*lBanTay*sa + ca*cr*lBanTay*sa + ca*ca*lBanTay*sb*sr,
  //lNgonTay*(saHp*(cb*sr + cr*sa*sb) - ca*caHp*sb) + lBanTay*sa*(cb*sr + cr*sa*sb) - ca*ca*cb*lBanTay*sb - ca*lBanTay*sb*(cb*cr - sa*sb*sr));
  /**ma tran nayA0*A222*A3*A4 cnug dc**/
  //translate(lNgonTayP*(saHp*(sb*sr - cb*cr*sa) + ca*caHp*cb) + ca*cb*lBanTay,
  //                  lBanTay*sa + lNgonTayP*(caHp*sa + ca*cr*saHp),
  //lNgonTayP*(saHp*(cb*sr + cr*sa*sb) - ca*caHp*sb) - ca*lBanTay*sb);

   //translate(lBanTay*ca*cb,lBanTay*sa,-ca*lBanTay*sb);
  //applyMatrix(saHp*(sb*sr - cb*cr*sa) - caHp*sbHp*(cr*sb + cb*sa*sr) + ca*caHp*cb*cbHp, (cr*sb + cb*sa*sr)*(cbHp*srHp + crHp*saHp*sbHp) + caHp*crHp*(sb*sr - cb*cr*sa) + ca*cb*(sbHp*srHp - cbHp*crHp*saHp), (cr*sb + cb*sa*sr)*(cbHp*crHp - saHp*sbHp*srHp) + ca*cb*(crHp*sbHp + cbHp*saHp*srHp) - caHp*srHp*(sb*sr - cb*cr*sa),0,
  //caHp*cbHp*sa + ca*cr*saHp + ca*caHp*sbHp*sr,                              sa*(sbHp*srHp - cbHp*crHp*saHp) - ca*sr*(cbHp*srHp + crHp*saHp*sbHp) + ca*caHp*cr*crHp,                              sa*(crHp*sbHp + cbHp*saHp*srHp) - ca*sr*(cbHp*crHp - saHp*sbHp*srHp) - ca*caHp*cr*srHp,0,
  //saHp*(cb*sr + cr*sa*sb) - caHp*sbHp*(cb*cr - sa*sb*sr) - ca*caHp*cbHp*sb, (cb*cr - sa*sb*sr)*(cbHp*srHp + crHp*saHp*sbHp) + caHp*crHp*(cb*sr + cr*sa*sb) - ca*sb*(sbHp*srHp - cbHp*crHp*saHp), (cb*cr - sa*sb*sr)*(cbHp*crHp - saHp*sbHp*srHp) - caHp*srHp*(cb*sr + cr*sa*sb) - ca*sb*(crHp*sbHp + cbHp*saHp*srHp),0,
  //0,0,0,1);
  //TA0*A2*A3*A444 khong can tranlate
  applyMatrix( saHp*(sb*sr - cb*cr*sa) + ca*caHp*cb, caHp*(sb*sr - cb*cr*sa) - ca*cb*saHp, cr*sb + cb*sa*sr, lNgonTayP*(saHp*(sb*sr - cb*cr*sa) + ca*caHp*cb) + ca*cb*lBanTay,
                 caHp*sa + ca*cr*saHp,                 ca*caHp*cr - sa*saHp,           -ca*sr,                    lBanTay*sa + lNgonTayP*(caHp*sa + ca*cr*saHp),
 saHp*(cb*sr + cr*sa*sb) - ca*caHp*sb, caHp*(cb*sr + cr*sa*sb) + ca*saHp*sb, cb*cr - sa*sb*sr, lNgonTayP*(saHp*(cb*sr + cr*sa*sb) - ca*caHp*sb) - ca*lBanTay*sb,
                                  0,                                  0,                0,                                                             1);
  
    pushMatrix();
    
  noStroke();
  lights();
  fill(255, 51, 0);
  sphere(rPoint);
  popMatrix();
}
